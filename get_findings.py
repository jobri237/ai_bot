import os
import requests
import json 
from dotenv import find_dotenv, load_dotenv

def get_dojo_findings():

    # Get the token, url of defectdojo and app name from environment variables
    load_dotenv()

    token = os.getenv('DEFECT_DOJO_TOKEN')
    app_name = os.getenv('PRODUCT_NAME')
    dojo_host = os.getenv('DOJO_HOST')

    if token is None or app_name is None:
        raise ValueError("Environment variables DEFECT_DOJO_TOKEN and APP must be set")

    if dojo_host is None:
        raise ValueError("Environment variables DOJO_HOST must be set")

    url = f"{dojo_host}/api/v2/findings/?product_name={app_name}&active=true&created=1&duplicate=false"
    headers = {
        'accept': 'application/json',
        'Authorization': f'Token {token}',
    }

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json() 
        
        cicd_data = data['results']

    else:
        print(f"Failed to get data: {response.status_code}")
        print(response.text)
    
    return cicd_data
