import requests
import os
from dotenv import find_dotenv, load_dotenv

def send_rec(mitigation_result, id):

    load_dotenv()
    dojo_host = os.getenv('DOJO_HOST')
    xcsrf = os.getenv('ASSISTANT_ID')
    token = os.getenv('DEFECT_DOJO_TOKEN')

    url = f"{dojo_host}/api/v2/findings/{id}/"
    
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRFTOKEN': xcsrf,
        'Authorization': f'Token {token}',
    }

    data = {
        "mitigation": mitigation_result
    }

    response = requests.patch(url, headers=headers, json=data)
    print(response.text)

    return None