import ai_bot
import os
import send_recom
import get_findings

issues = get_findings.get_dojo_findings()
number_of_issues = len(issues)
x=1
commit = os.getenv('CI_COMMIT_SHORT_SHA')

if not issues or 'results' not in issues:
    print(f"They are no new issues in the current commit: {commit} ")
    exit(0)

for issue in issues:
    id = issue['id']
    title = issue['title']
    # cwe = issue['cwe']
    # mitigation = issue['mitigation']
    description = issue['description']
    issue_descr = title + ':: ' + description
    print(f"Number of issues to mitigate: {number_of_issues}")
    print(f"*********Processing issue: {x}**************") 
    x+=1
# Call our sec_bot to retrieve the issues and recommmend mitigations    
    mitigation_result=ai_bot.sec_bot(issue_descr, total_nbr_of_issues=number_of_issues)

# Call our send_rec bot to send mitigations proposed by chatGPT to DefectDojo
    send_recom.send_rec(mitigation_result, id) 

print("======= All issues where analyzed and mitigation steps where provided, Check your vulnerability management platform =======")