from openai import OpenAI
from dotenv import find_dotenv, load_dotenv
import time
import os
import logging
from datetime import datetime

def sec_bot(issue_to_mitigate, sleep_interval=5, total_nbr_of_issues=1):
    """

    Waits for a run to complete and prints the elapsed time.:param client: The OpenAI client object.
    :param thread_id: The ID of the thread.
    :param run_id: The ID of the run.
    :param sleep_interval: Time in seconds to wait between checks.
    """
    load_dotenv()

    client = OpenAI(
      api_key=os.getenv('OPENAI_API_KEY')
    )

    assistant_id = value = os.getenv('ASSISTANT_ID')
    thread_id = value = os.getenv('THREAD_ID')

    # === Create a message ======================

    message = issue_to_mitigate
    message = client.beta.threads.messages.create(
        thread_id=thread_id,
        role="user",
        content=message
    )

    # === Run our assistant =====================

    run = client.beta.threads.runs.create(
        thread_id=thread_id,
        assistant_id=assistant_id,
        instructions="sign your recommendations with 'Powered By OpenAI'"
    )
    run_id=run.id
    print("Run started")

    x = 1
    limit = total_nbr_of_issues

    while x <= limit:
        try:
            run = client.beta.threads.runs.retrieve(thread_id=thread_id, run_id=run_id)
            if run.completed_at:
                elapsed_time = run.completed_at - run.created_at
                formatted_elapsed_time = time.strftime(
                    "%H:%M:%S", time.gmtime(elapsed_time)
                )
                print(f"Run completed in {formatted_elapsed_time}")
                logging.info(f"Run completed in {formatted_elapsed_time}")
                # Get messages here once Run is completed!
                messages = client.beta.threads.messages.list(thread_id=thread_id)
                last_message = messages.data[0]
                response = last_message.content[0].text.value
                print(f"Assistant Response: {response} \r======================================================================================================================")
                break
            x += 1
        except Exception as e:
            logging.error(f"An error occurred while retrieving the run: {e}")
            break
        logging.info("Waiting for run to complete...")
        time.sleep(sleep_interval)
    return response

